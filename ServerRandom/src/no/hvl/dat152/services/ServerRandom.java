package no.hvl.dat152.services;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/random") 
public class ServerRandom {
	private ScheduledExecutorService timer;

    
    @OnOpen
    public void showRandom(Session session) {
        System.out.println(session.getId() + " has opened a connection");
    	timer = Executors.newSingleThreadScheduledExecutor();
        Random randomGenerator = new Random();
        timer.scheduleAtFixedRate(() -> sendRandom(session,randomGenerator),0,5,TimeUnit.SECONDS);
    }
        
    private void sendRandom(Session session,Random randomGenerator) {
    	String randomNumberAsText = Integer.toString(randomGenerator.nextInt(100));
    	System.out.println("To client: " + randomNumberAsText + ".");
    	try {
			session.getBasicRemote().sendText(randomNumberAsText);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
 
    @OnClose
    public void onClose(Session session){
    	timer.shutdown();
        System.out.println("Session " +session.getId()+" has ended");
    }
}
