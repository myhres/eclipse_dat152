'use strict';

class AppController {
	constructor(memberlist, addMember) {
		this.memberlist = memberlist
		this.addMember = addMember
	}

	run() {
		this.ui = new UIHandler()
		document.getElementById(this.memberlist).appendChild(this.ui.UIElement)

		// Setter callback-metodene i uiHandler
		this.ui.deleteMemberCallback = this.deleteMemberAJAX.bind(this)
		this.ui.editMemberCallback = this.editMemberAJAX.bind(this)
		this.ui.newMemberCallback = this.newMemberAJAX.bind(this)

		// Må kalle denne hver gang siden lastes på nytt
		this.getAllMembers()

		// debug
		this.updateFrequency = 3000
		// hvert 3. sek, 500ms delay første gang
		setTimeout(setInterval, 500, this.getUpdates.bind(this), this.updateFrequency)
	}

	// Metode som leter etter oppdaterte/nye medlem
	// og deretter ber UI om å fjerne de fra tabellen
	getUpdates() {
		const xhr = new XMLHttpRequest()

		xhr.addEventListener('loadend', e => {
			const xmlhttp = e.target

			if (xmlhttp.status === 200) {
				const { newMembers, updatedMembers, deletedMembers } = JSON.parse(xmlhttp.responseText)
				const fetchedLogId = JSON.parse(xmlhttp.responseText).logId - 1 // hent nyeste logId

				// Oppdater view dersom vi har fått en ny logId
				if (this.lastLogId !== fetchedLogId) {

					// Sletter medlem, dersom det er noen å slette
					if (deletedMembers) {
						deletedMembers.forEach(memberId => {
							const rowId = this.ui.findRowId(memberId)
							this.ui.removeMemberFromTable(rowId)
						})
					}

					// Legger til nye medlem, dersom det er noen nye
					if (newMembers) {
						newMembers.forEach(member => {
							this.ui.addMember(member)
						})
					}

					// Oppdater medlemmer
					if (updatedMembers) {
						updatedMembers.forEach(member => { this.ui.updateMember(member) })
					}
				}

				this.lastLogId = fetchedLogId // Oppdaterer siste logId
			} else {
				console.error('Error: ' + e.target.status)
			}
		}, true)

		if (this.lastLogId === undefined) this.lastLogId = -1
		xhr.open('GET', `/Mservices/data/updates/${this.lastLogId}`, true)
		xhr.send(null)
	}

	// Gets all members from server, and adds to UI
	getAllMembers() {
		const xhr = new XMLHttpRequest()

		// Etter svar fra server legges de til i UI
		xhr.addEventListener('loadend', e => {
			const xmlhttp = e.target

			if (xmlhttp.status === 200) {
				const newMembers = JSON.parse(xmlhttp.responseText).newMembers
				newMembers.forEach(member => { this.ui.addMember(member) })

				// Oppdaterer siste log id
				this.lastLogId = JSON.parse(xmlhttp.responseText).logId - 1
			} else {
				console.error('Error: ' + xmlhttp.status)
			}
		}, true)

		// Henter alle updates
		xhr.open('GET', '/Mservices/data/updates/-1', true)
		xhr.send(null)
	}

	// Metode som kjøres når 'delete' trykkes
	deleteMemberAJAX(id) {
		const xhr = new XMLHttpRequest()

		// Etter at klienten har fått svar fra serveren
		// kjøres getUpdates
		xhr.addEventListener('loadend', e => {
			const xmlhttp = e.target

			if (xmlhttp.status === 200) {
				this.getUpdates()
			} else {
				console.error('Error: ' + xmlhttp.status)
			}
		}, true)

		xhr.open('DELETE', `/Mservices/data/member/${id}`, true)
		xhr.send(null)
	}

	// Metode som kjøres når 'edit' trykkes
	editMemberAJAX(member) {
		const xhr = new XMLHttpRequest()

		xhr.addEventListener('loadend', e => {
			const xmlhttp = e.target

			if (xmlhttp.status === 200) {
				this.getUpdates()
			} else {
				console.error('Error: ' + xmlhttp.status)
			}
		}, true)

		xhr.open('PUT', `/Mservices/data/member/${member.memberId}`, true)
		xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
		xhr.send(JSON.stringify(member)) // Send JSON
	}

	// Legger til et nytt member i databasen
	newMemberAJAX(member) {
		const xhr = new XMLHttpRequest()

		xhr.addEventListener('loadend', e => {
			const xmlhttp = e.target

			if (xmlhttp.status === 200) {
				this.getUpdates()
			} else {
				console.error('Error: ' + xmlhttp.status)
			}
		}, true)

		xhr.open('POST', `/Mservices/data/member`, true)
		xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
		xhr.send(JSON.stringify(member)) // Send JSON
	}
}

const app = new AppController('memberlist', 'addMember')
document.addEventListener('DOMContentLoaded', app.run.bind(app), true)