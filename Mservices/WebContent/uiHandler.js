'use strict';

class UIHandler {
  constructor() {
    // Create table element
    this.UIElement = document.createElement('table')

    // Create header
    const header = this.UIElement.createTHead()
    const row = header.insertRow(0)
    row.insertCell(-1).outerHTML = '<th>Firstname</th>'
    row.insertCell(-1).outerHTML = '<th>Lastname</th>'
    row.insertCell(-1).outerHTML = '<th>Address</th>'
    row.insertCell(-1).outerHTML = '<th>Phone</th>'

    // Create body
    this.UIElement.createTBody()

    // Add member button
    const addMemberBtn = document.getElementById('addMember')
    addMemberBtn.addEventListener('click', this.newMember())
  }

  // Legger til et medlem i tabellen, dersom den ikke er der fra før
  addMember(member) {
    if(!this.memberExists(member)) {
      // Create row
      const row = this.UIElement.tBodies[0].insertRow(-1) // get first body and insert row

      // Add member to table
      row['data-memberId'] = member.memberId // Lagrer memberId i en skjult attributt
      row.insertCell(-1).textContent = member.firstname
      row.insertCell(-1).textContent = member.lastname
      row.insertCell(-1).textContent = member.address
      row.insertCell(-1).textContent = member.phone

      // Add buttons
      // Delete button
      const deleteBtn = document.createElement('button')
      deleteBtn.innerHTML = 'Delete'
      deleteBtn.addEventListener('click', () => { this.deleteCallback(row['data-memberId']) })
      row.insertCell(-1).appendChild(deleteBtn)

      // Edit button
      const editBtn = document.createElement('button')
      editBtn.innerHTML = 'Edit'
      editBtn.addEventListener('click', () => { this.editMember(member) })
      row.insertCell(-1).appendChild(editBtn)
    }
  }

  // Sjekker om member allerede finnes i tabellen
  memberExists(member) {
    if (member) {
      return Array.from(this.UIElement.rows).some(row => {
        return row['data-memberId'] === member.memberId
      })
    }
    return undefined
  }

  // Returnerer rowId til member eller undefined
  findRowId(memberId) {
    if (memberId) {
      const row = Array.from(this.UIElement.rows).find(row => {
        return row['data-memberId'] === memberId
      })

      // Returnerer rowId dersom den fant den
      if (row) {
        return row.rowIndex
      } else {
        return undefined
      }
    }
    return undefined
  }

  // Fjerner en member fra tabellen, dersom den finnes
  removeMemberFromTable(rowIndex) {
    if (rowIndex) {
      this.UIElement.deleteRow(rowIndex)
    }
  }

  // bindes til controller som gjør AJAX
  set editMemberCallback(cb) {
    this.editCallback = cb
  }

  // bindes til controller som gjør AJAX
  set deleteMemberCallback(cb) {
    this.deleteCallback = cb
  }

  // bindes til controller som gjør AJAX
  set newMemberCallback(cb) {
    this.newCallback = cb
  }

  // Oppdaterer et medlem
  updateMember(member) {
    const rowId = this.findRowId(member.memberId)
    const tableCells = this.UIElement.rows.item(rowId).getElementsByTagName('td')
    
    // Finner medlem i tabell og bytter ut med ny verdi
    tableCells[0].textContent = member.firstname
    tableCells[1].textContent = member.lastname
    tableCells[2].textContent = member.address
    tableCells[3].textContent = member.phone
  }

  // Gjør det mulig å endre på en member
  editMember(member) {
    const field = document.getElementById('editField')
    const inputs = field.getElementsByTagName('input')
    const button = field.getElementsByTagName('button')[0]

    // Sett inputs til valgt member
    inputs[0].value = member.firstname
    inputs[1].value = member.lastname
    inputs[2].value = member.address
    inputs[3].value = member.phone

    // Kloner og bytter ut knappen for å slette listeners
    const newButton = button.cloneNode(true)
    button.parentNode.replaceChild(newButton, button)

    // Lagre når 'Update member' trykkes
    newButton.addEventListener('click', () => { 
      member.firstname = inputs[0].value
      member.lastname = inputs[1].value
      member.address = inputs[2].value
      member.phone = inputs[3].value

      this.editCallback(member) // AJAX
    })
  }

  // Gjør det mulig å skrive inn nytt member
  newMember() {
    const field = document.getElementById('editField')
    const inputs = field.getElementsByTagName('input')
    const button = field.getElementsByTagName('button')[0]

    // Sletter innholdet i inputene
    Array.from(inputs).forEach(input => { input.value = '' })
    
    // Kloner og bytter ut knappen for å slette listeners
    const newButton = button.cloneNode(true)
    button.parentNode.replaceChild(newButton, button)
    // button.removeEventListener('click')

    // Lagre når 'Update member' trykkes
    newButton.addEventListener('click', () => { 
      this.newCallback({
        firstname: inputs[0].value,
        lastname: inputs[1].value,
        address: inputs[2].value,
        phone: inputs[3].value
      })
    })
  }

  clearFieldInputs() {
    const field = document.getElementById('editField')
    const inputs = field.getElementsByTagName('input')

    Array.from(inputs).forEach(field => { field.value = '' })
  }

  // Returnerer antall rader i tabellen
  get length() {
    return this.UIElement.rows.length - 1
  }
}