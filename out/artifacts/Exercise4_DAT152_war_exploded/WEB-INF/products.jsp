<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="dat152-libs" prefix="dat152" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Products</title>
	</head>
	<body>
    <fmt:setLocale value="${ currentLocale }"/>
    <dat152:localeSwitcher locales="${ locales }"/>
		<fmt:setBundle basename="descriptions" var="des"/>
    <fmt:setBundle basename="apptexts" var="app"/>
		<c:forEach items="${ products }" var="product">
			<h3>${ product.getpName() }</h3>
			<table>
				<fmt:bundle basename="apptexts">
					<tr>
						<td rowspan="4"><img src="${ product.getImageFile() }"/></td>
						<td>
							<fmt:message key="name"/>: 
							<c:out value="${ product.getpName() }"/>
						</td>
					</tr>
					<tr>
						<td>
							<fmt:message key="price"/>:
              <fmt:formatNumber type="currency">
                <dat152:currencyConverter toCurrency="${ currency }" amount="${ product.getPriceInEuro() }"/>
              </fmt:formatNumber>
						</td>
					</tr>
					<tr>
						<td>
							<fmt:message key="description"/>: 
							<fmt:message key="${ product.getPno() }" bundle="${ des }"/>
						</td>
					 </tr>
					<tr>
						<td>
              <%-- kunne kanskje gjort dette annerledes med en database/EJB --%>
              <form method="post">
                <input type="text" name="item" value="${ product.toString() }" style="display:none;">
                <c:set var="addToCart"><fmt:message key="addToCart"/></c:set>
                <input type="submit" value="${ addToCart }">
              </form><%-- POST to cart --%>
						</td>
					</tr>
				</fmt:bundle> 
			</table>
		</c:forEach>
    <a href="home"><fmt:message key="home" bundle="${ app }"/></a>
    <a href="cart"><fmt:message key="cart" bundle="${ app }"/></a>
    <p><dat152:copyright since="2013">Høgskolen på Vestlandet</dat152:copyright></p>
	</body>
</html>