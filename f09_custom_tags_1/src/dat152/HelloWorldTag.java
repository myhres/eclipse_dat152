package dat152;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * Tag that write the text "Hello World!" on a page
 * @author sig
 */
public class HelloWorldTag extends SimpleTagSupport {
	@Override
	public void doTag()throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		out.println("Hello World!");
	}
}
