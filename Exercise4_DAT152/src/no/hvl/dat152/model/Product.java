package no.hvl.dat152.model;

import javax.ejb.Stateful;

public class Product {
	private static int count = 0; // Har bare en enkel inkrement for pno

	private int pno;
	private String pName;
	private double priceInEuro;
	private String imageFile;
	
	public Product() {
		this.pno = count++;
	}
	
	public String getpName() {
		return pName;
	}
	
	public void setpName(String pName) {
		this.pName = pName;
	}
	
	public double getPriceInEuro() {
		return priceInEuro;
	}
	
	public void setPriceInEuro(double priceInEuro) {
		this.priceInEuro = priceInEuro;
	}
	
	public String getImageFile() {
		return imageFile;
	}
	
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	public String toString() {
		return "pno:" + pno
				+ "_pName:" + pName
				+ "_priceInEuro:" + priceInEuro
				+ "_imageFile:" + imageFile;
	}
	
	// Read only
	public int getPno() {
		return pno;
	}
}
