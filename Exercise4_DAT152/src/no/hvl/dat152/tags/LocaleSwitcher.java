package no.hvl.dat152.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocaleSwitcher extends SimpleTagSupport {
    private List<Locale> locales;

    public final void setLocales(List<Locale> locales) {
        this.locales = locales;
    }

    @Override
    public final void doTag() throws JspException, IOException {
        PageContext pc = (PageContext) getJspContext();
        JspWriter out = pc.getOut();

        for(Locale locale : locales) {
            out.print("<a href=\"?lang=" + locale.toString() + "\">");
            out.print(locale.getDisplayLanguage(locale));
            out.println("</a>");
        }
    }
}
