package no.hvl.dat152.tags;

import no.hvl.dat152.utils.StringUtil;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class ShortText extends SimpleTagSupport{
    private int maxchars;

    public final void setMaxchars(final int maxchars) {
        this.maxchars = maxchars;
    }

    @Override
    public final void doTag() throws JspException, IOException {
        PageContext pc = (PageContext) getJspContext();
        JspWriter out = pc.getOut();

        StringWriter sw = new StringWriter();
        getJspBody().invoke(sw);

        /*
        System.out.println(StringUtil.shortText(sw.toString(), this.maxchars));
        System.out.println(sw.toString());
        */
        out.println(StringUtil.shortText(sw.toString(), this.maxchars));
    }
}
