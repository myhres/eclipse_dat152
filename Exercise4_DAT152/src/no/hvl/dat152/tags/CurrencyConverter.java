package no.hvl.dat152.tags;

import no.hvl.dat152.utils.ExchangeCurrency;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Currency;

public class CurrencyConverter extends SimpleTagSupport {
    private double amount;
    private Currency toCurrency;

    public final void setAmount(double amount) {
        this.amount = amount;
    }

    public void setToCurrency(Currency toCurrency) {
        this.toCurrency = toCurrency;
    }

    @Override
    public final void doTag() throws JspException, IOException {
        amount = ExchangeCurrency.fromEUR(toCurrency, amount);

        PageContext pc = (PageContext) getJspContext();
        JspWriter out = pc.getOut();

        out.print(amount);
    }
}
