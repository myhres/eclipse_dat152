package no.hvl.dat152.tags;

import no.hvl.dat152.utils.StringUtil;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class Copyright extends SimpleTagSupport {
    private int since;

    public final void setSince(final int since) {
        this.since = since;
    }


    @Override
    public final void doTag() throws JspException, IOException {
        PageContext pc = (PageContext) getJspContext();
        JspWriter out = pc.getOut();

        out.print(StringUtil.copyright("", this.since));
        getJspBody().invoke(out);
    }
}
