package no.hvl.dat152.controller;

import no.hvl.dat152.utils.CookieUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Locale currentLocale;
	private List<Locale> locales;
       
    public HomeServlet() {
        super();
    }

    @Override
    public void init() throws ServletException {
        // Get locales from web.xml
        locales = new ArrayList<>();

        for(String locale : getServletContext()
                .getInitParameter("locales").split(",")) {
            locales.add(new Locale(locale.split("_")[0], locale.split("_")[1]));
        }
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        request.setCharacterEncoding("UTF-8");

        // Locale as get param (from lang switcher)
        Locale currentLocale;
        if(request.getParameter("lang") != null) {
            String lang = request.getParameter("lang");
            currentLocale = new Locale(lang.split("_")[0], lang.split("_")[1]);
            response.addCookie(new Cookie("locale", lang));

            // Går tilbake til samme side, uten get params
            //doGet(request, response);
            //request.getRequestDispatcher("home").forward(request, response);
        }
        else { // Locale from cookies
            currentLocale = CookieUtils.getCookieLocale(request);
            if(currentLocale != null) {
                request.setAttribute("currentLocale", currentLocale);
            }
            else { // Locale from browser
                System.out.println("browser");//temp
            }
        }

        request.setAttribute("currentLocale", currentLocale.toString());
		request.setAttribute("locales", locales);

		request.getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
