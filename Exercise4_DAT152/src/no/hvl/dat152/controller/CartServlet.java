package no.hvl.dat152.controller;

import no.hvl.dat152.model.Cart;
import no.hvl.dat152.model.Product;
import no.hvl.dat152.tags.CurrencyConverter;
import no.hvl.dat152.utils.CookieUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/cart")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<Locale> locales;
	private Cart cart;
       
    public CartServlet() {
        super();
    }

    @Override
	public void init() {
		// Get locales from web.xml
		locales = new ArrayList<>();

		for(String locale : getServletContext()
				.getInitParameter("locales").split(",")) {
			locales.add(new Locale(locale.split("_")[0], locale.split("_")[1]));
		}

		// CartServlet mottar/leser en Cart
        Product cup = new Product();
		cup.setpName("White Coffee Cup");
		cup.setImageFile("img/cup.jpg");
		cup.setPriceInEuro(6.50);

		Product bottle = new Product();
		bottle.setpName("Blue Water Bottle");
		bottle.setImageFile("img/bottle.jpg");
		bottle.setPriceInEuro(3.50);

		cart = new Cart();
		cart.addItem(cup);
		cart.addItem(cup);
		cart.addItem(bottle);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");

		// Locale as get param (from lang switcher)
		Locale currentLocale;
		if(request.getParameter("lang") != null) {
			String lang = request.getParameter("lang");
			currentLocale = new Locale(lang.split("_")[0], lang.split("_")[1]);
			response.addCookie(new Cookie("locale", lang));

			// Går tilbake til samme side, uten get params
			//doGet(request, response);
			//request.getRequestDispatcher("home").forward(request, response);
		}
		else { // Locale from cookies
			currentLocale = CookieUtils.getCookieLocale(request);
			if(currentLocale != null) {
				request.setAttribute("currentLocale", currentLocale);
			}
			else { // Locale from browser
				System.out.println("browser");//temp
			}
		}

		request.setAttribute("currentLocale", currentLocale.toString());
		request.setAttribute("currency", Currency.getInstance(currentLocale));
		request.setAttribute("locales", locales);
		request.setAttribute("items", cart.getItems());
		request.setAttribute("cart", cart);

		request.getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
