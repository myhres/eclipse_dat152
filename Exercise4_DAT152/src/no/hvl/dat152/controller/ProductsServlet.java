package no.hvl.dat152.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.hvl.dat152.model.*;
import no.hvl.dat152.utils.CookieUtils;

@WebServlet("/products")
public class ProductsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<Product> products;
	private List<Locale> locales;
	private Cart cart;

    public ProductsServlet() {
        super();
    }
    
    @Override
    public void init() throws ServletException {
		// Get locales
        locales = new ArrayList<>();

        for(String locale : getServletContext()
        		.getInitParameter("locales").split(",")) {
        	locales.add(new Locale(locale.split("_")[0], locale.split("_")[1]));
        }

        // Products/mock
        products = new ArrayList<>();
        
		Product cup = new Product();
		cup.setpName("White Coffee Cup");
		cup.setImageFile("img/cup.jpg");
		cup.setPriceInEuro(6.50);

		Product bottle = new Product();
		bottle.setpName("Blue Water Bottle");
		bottle.setImageFile("img/bottle.jpg");
		bottle.setPriceInEuro(3.50);
		
		products.add(cup);
		products.add(bottle);

		// Init cart
		cart = new Cart();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");

		// Locale as get param (from lang switcher)
		Locale currentLocale;
		if(request.getParameter("lang") != null) {
			String lang = request.getParameter("lang");
			currentLocale = new Locale(lang.split("_")[0], lang.split("_")[1]);
			response.addCookie(new Cookie("locale", lang));

			// Går tilbake til samme side, uten get params
			//doGet(request, response);
			//request.getRequestDispatcher("home").forward(request, response);
		}
		else { // Locale from cookies
			currentLocale = CookieUtils.getCookieLocale(request);
			if(currentLocale != null) {
				request.setAttribute("currentLocale", currentLocale);
			}
			else { // Locale from browser
				System.out.println("browser");//temp
			}
		}

		request.setAttribute("currentLocale", currentLocale.toString());
		request.setAttribute("currency", Currency.getInstance(currentLocale));
		request.setAttribute("products", products);
		request.setAttribute("locales", locales);

		request.getRequestDispatcher("/WEB-INF/products.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/**
		 * Her ville controlleren mottatt product fra view.
		 * Deretter PRG/get mot products.
		 */

		doGet(request, response);
	}
}
