package no.hvl.dat152.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class CookieUtils {
    /**
     * Metode som finner locales i cookies, og returnerer
     * locale.
     * @param request from servlet
     * @return Locale from cookies
     */
    public static Locale getCookieLocale(HttpServletRequest request) {
        List<Cookie> cookies = new ArrayList<>();
        cookies.addAll(Arrays.asList(request.getCookies()));

        // Returnerer et Locale fra cookies
        for(Cookie cookie : cookies) {
            if(cookie.getName().equalsIgnoreCase("locale")) {
                return new Locale(cookie.getValue().split("_")[0],
                        cookie.getValue().split("_")[1]);
            }
        }

        return null;
    }

    /**
     * Returnerer response med locale i cookies.
     * @param request som inneholder URL parameter
     * @param response
     * @return response som inneholder cookies
     */
    @Deprecated
    public static HttpServletResponse localeUrlToCookies(HttpServletRequest request, HttpServletResponse response) {
        String lang = request.getParameter("lang");
        response.addCookie(new Cookie("locale", lang));

        return response;
    }
}
