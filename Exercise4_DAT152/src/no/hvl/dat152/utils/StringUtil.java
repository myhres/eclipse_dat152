package no.hvl.dat152.utils;

import java.util.Calendar;

public class StringUtil {

	public static String shortText(String text, int maxchars) {
		if(text.length() < maxchars) {
			return text;
		} else {
			return text.substring(0, maxchars) + " ...";
		}
	}
	
	public static String copyright(String text, int since) {
		return "© " + romanNumeral(since) + "-" 
				+ romanNumeral(Calendar.getInstance().get(Calendar.YEAR))
				+ " " + text;
	}
	
	// Lånt fra: https://stackoverflow.com/a/39411250/6002302
	public static String romanNumeral(int number) {
	    return nCopies(number, "I")
				.replace("IIIII", "V")
	            .replace("IIII", "IV")
	            .replace("VV", "X")
	            .replace("VIV", "IX")
	            .replace("XXXXX", "L")
	            .replace("XXXX", "XL")
	            .replace("LL", "C")
	            .replace("LXL", "XC")
	            .replace("CCCCC", "D")
	            .replace("CCCC", "CD")
	            .replace("DD", "M")
	            .replace("DCD", "CM");
	}
	
	private static String nCopies(int count, String string) {
		String result = "";

		for(int i = 0; i < count; i++) {
			result += string;
		}
		
		return result;
	}

	public static String capitalize(String string) {
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}

	public static String escapeHTML(String html) {

		String result = "";
		for(int i = 0; i < html.length(); i++) {
			switch(html.charAt(i)) {
				case ' ':
				    result += "&nbsp;";
					break;
				case '&':
					result += "&amp;";
					break;
				case '"':
					result += "&quot;";
					break;
				case '<':
					result += "&lt;";
					break;
				case '>':
					result += "&gt;";
					break;
				case '\'':
					result += "&#39;";
					break;
				default:
					result += html.charAt(i);
					break;
			}
		}

		return result;
	}
}
