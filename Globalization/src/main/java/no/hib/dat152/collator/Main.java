package no.hib.dat152.collator;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

import no.hib.dat152.model.Employee;
import no.hib.dat152.model.EmployeeComparator;

/**
 * The main app.
 *
 * @author Atle Geitung
 * @author Lars-Petter Helland
 */
public final class Main {

    /**
     * Hides the constructor.
     */
    private Main() {
    }

    /**
     * The main app.
     *
     * @param args Not used.
     */
    public static void main(final String[] args) {

        Employee[] employees = { new Employee("Nilsen   ", 34567.00), new Employee("Ytrebygd ", 35567.00),
                new Employee("Aaseb�   ", 36567.00), new Employee("�vreb�   ", 32567.00), new Employee("Abelsen  ", 38567.00),
                new Employee("�lvik    ", 30567.00) };

        Locale locale = new Locale("en", "US");
        //Locale locale = new Locale("no", "NO");

        EmployeeComparator comp = new EmployeeComparator(locale);
        Arrays.sort(employees, comp);

        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);

        System.out.println("Name\t\tSalary");
        System.out.println("----------------------------");
        for (Employee employee : employees) {
            System.out.println(employee.getName() + " " + formatter.format(employee.getSalary()));
        }
    }

}
