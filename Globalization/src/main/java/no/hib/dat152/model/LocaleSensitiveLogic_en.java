package no.hib.dat152.model;

import java.util.ListResourceBundle;
import java.util.Locale;

/**
 * Locate kelvin to English.
 *
 * @author Atle Geitung
 */
public class LocaleSensitiveLogic_en extends ListResourceBundle {

    @Override
    protected final Object[][] getContents() {

        Object[][] oo = { { "tempformat", new KelvinToFahrenheitString(new Locale("en")) } };
        return oo;
    }

}
