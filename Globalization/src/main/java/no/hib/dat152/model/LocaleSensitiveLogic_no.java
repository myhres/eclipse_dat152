package no.hib.dat152.model;

import java.util.ListResourceBundle;
import java.util.Locale;

/**
 * Locate kelvin to Norwegian.
 *
 * @author Atle Geitung
 */
public class LocaleSensitiveLogic_no extends ListResourceBundle {

    @Override
    protected final Object[][] getContents() {

        Object[][] oo = { { "tempformat", new KelvinToCelsiusString(new Locale("no")) } };
        return oo;
    }

}
