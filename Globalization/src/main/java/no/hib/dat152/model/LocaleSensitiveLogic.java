package no.hib.dat152.model;

import java.util.ListResourceBundle;

/**
 * Locate kelvin using default locale.
 *
 * @author Atle Geitung
 */
public class LocaleSensitiveLogic extends ListResourceBundle {

    @Override
    protected final Object[][] getContents() {

        Object[][] oo = { { "tempformat", new KelvinToKelvinString() } };
        return oo;
    }

}
