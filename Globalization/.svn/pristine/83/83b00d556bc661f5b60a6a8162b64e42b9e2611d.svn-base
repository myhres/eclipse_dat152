package no.hib.dat152.modell;

import java.util.Locale;

/**
 * Klasse for � formatere en grad i kelvin som celcius.
 *
 * @author Atle Geitung
 * @author Lars-Petter Helland
 */
public class KelvinToCelsiusString implements KelvinToString {

    private static final int CELSIUS_NULL = 273;

    private Locale locale;

    /**
     * Lager et nytt konverteringsobjekt.
     */
    public KelvinToCelsiusString() {
    }

    /**
     * Lager et nytt konverteringsobjekt.
     *
     * @param locale locale
     */
    public KelvinToCelsiusString(final Locale locale) {
        this.locale = locale;
    }

    /**
     * {@inheritDoc}
     *
     * @see no.hib.dat152.modell.KelvinToString#format(Double)
     */
    @Override
    public final String format(final Double kelvin) {
        double celsius = kelvin - CELSIUS_NULL;
        return String.format(locale, "%.1f\u00B0 Celsius", celsius);
    }

    /**
     * {@inheritDoc}
     *
     * @see no.hib.dat152.modell.KelvinToString#setLocale(Locale)
     */
    @Override
    public final void setLocale(final Locale locale) {
        this.locale = locale;
    }

}
