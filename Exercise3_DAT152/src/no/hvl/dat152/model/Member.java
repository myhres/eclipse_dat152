package no.hvl.dat152.model;

public class Member {
	private String firstname;
	private String lastname;
	private String address;
	private String phone;

	public Member(String fname, String lname, String address, String phone) {
		this.firstname = fname;
		this.lastname = lname;
		this.address = address;
		this.phone = phone;
	}
	
	public String getFirstName() {
		return this.firstname;
	}
	
	public String getLastName() {
		return this.lastname;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public String getPhone() {
		return this.phone;
	}
}
