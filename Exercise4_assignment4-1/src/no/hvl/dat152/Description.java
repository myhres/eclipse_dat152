package no.hvl.dat152;

import java.util.Locale;

public class Description {
	private String text;
	private Locale langCode;
	private int pno;
	
	public Description(String text, Locale langCode, int productNumber) {
		this.text = text;
		this.langCode = langCode;
		this.setPno(productNumber);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Locale getLangCode() {
		return langCode;
	}

	public void setLangCode(Locale langCode) {
		this.langCode = langCode;
	}

	public int getPno() {
		return pno;
	}

	public void setPno(int pno) {
		this.pno = pno;
	}
}
