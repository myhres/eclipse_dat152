package no.hvl.dat152;

public class Product {
	private static int count = 0;

	private int pno;
	private String pName;
	private double priceInEuro;
	private String imageFile;
	
	public Product() {
		this.pno = count++;
	}
	
	public String getpName() {
		return pName;
	}
	
	public void setpName(String pName) {
		this.pName = pName;
	}
	
	public double getPriceInEuro() {
		return priceInEuro;
	}
	
	public void setPriceInEuro(double priceInEuro) {
		this.priceInEuro = priceInEuro;
	}
	
	public String getImageFile() {
		return imageFile;
	}
	
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}
	
	// Read only
	public int getPno() {
		return pno;
	}
}
