package no.hvl.dat152;

import java.util.Locale;

public class Runner {
	public static void main(String[] args) {
		Store store = new Store();

		// Legger til og velger locale
		store.addLocale(new Locale("en", "US"));
		store.addLocale(new Locale("nb", "NO"));
		store.addLocale(new Locale("de", "DE"));
//		store.setLocale(new Locale("de", "DE"));
//		store.setLocale(new Locale("en", "US"));
		store.setLocale(new Locale("nb", "NO"));
		
		// Oppretter produkt
		Product cup = new Product();
		cup.setpName("White Coffee Cup");
		cup.setImageFile("/cup.png");
		cup.setPriceInEuro(6.50);

		Product bottle = new Product();
		bottle.setpName("Big Water Bottle");
		bottle.setImageFile("/bottle.png");
		bottle.setPriceInEuro(3.50);

		// Legger til produktene i butikken
		store.addProduct(cup);
		store.addProduct(bottle);
		
		// Lister produkt og locales
		store.listProducts();
//		store.listLocales();
	}
}
