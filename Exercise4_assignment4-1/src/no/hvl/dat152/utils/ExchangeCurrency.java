package no.hvl.dat152.utils;

import java.util.Currency;

/**
 * En klasse som tar for seg omgjøring mellom valuta.
 * Den kan kobles opp til en API og hente valutakurser i sanntid,
 * men i denne oppgaven har jeg valgt å hardkode kursene.
 * 
 * @author Sigbjørn Myhre
 */
public class ExchangeCurrency {
	private static final double EUR_TO_USD = 1.17490;
	private static final double EUR_TO_NOK = 9.35232;

	public static double fromEUR(Currency currency, double amount) {
		if(currency.getCurrencyCode().equals("USD")) {
			return amount * EUR_TO_USD;
		} else if(currency.getCurrencyCode().equals("NOK")) {
			return amount * EUR_TO_NOK;
		} else {
			return amount;
		}
	}
}
