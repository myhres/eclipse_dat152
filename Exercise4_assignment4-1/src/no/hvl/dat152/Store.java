package no.hvl.dat152;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import no.hvl.dat152.utils.ExchangeCurrency;

public class Store {
	private List<Locale> locales; // Available locales in app
	private List<Product> products;
	private List<Description> descriptions; // Fjerne?
	private Locale displayLocale;
	private ResourceBundle apptexts;

	public Store() {
		locales = new ArrayList<Locale>();
		products = new ArrayList<Product>();
		descriptions = new ArrayList<Description>();
	}
	
	// Legger til nytt produkt
	public void addProduct(Product product) {
		products.add(product);
	}
	
	// Legger til nytt tilgjengelig locale
	public void addLocale(Locale locale) {
		locales.add(locale);
	}
	
	@Deprecated
	public void addDescription(Description description) {
		descriptions.add(description);
	}
	
	// Setter et valgt locale og henter tilsvarende apptexts
	public void setLocale(Locale locale) {
		displayLocale = locale;
		
		this.apptexts = ResourceBundle.getBundle("apptexts", displayLocale);
	}

	// Returnerer valgt locale
	public Locale getLocale() {
		return displayLocale;
	}
	
	// Lister alle produkt i butikken
	public void listProducts() {
		Currency currency = Currency.getInstance(displayLocale);

		for(Product p : products) { // resource bundle stuff
			System.out.println(apptexts.getString("name") + ": " + p.getpName());
			System.out.println(apptexts.getString("price") + ": " 
					+ NumberFormat.getCurrencyInstance(displayLocale)
					.format(ExchangeCurrency.fromEUR(currency, p.getPriceInEuro())));
			System.out.println(apptexts.getString("description") 
					+ ": " + apptexts.getString(p.getPno() + "_description")); // bedre måte?
			System.out.println("-------------------------------------");
		}
	}
	
	// Lister alle Locales i butikken
	public void listLocales() {
		for(Locale l : locales) {
			System.out.println("Locale: " + l);
		}

		System.out.println("Displaylocale: " + displayLocale);
	}
	
	@Deprecated
	private Description findDescription(Product product) {
		for(Description d : descriptions) {
			if (product.getPno() == d.getPno() 
					&& this.displayLocale.equals(d.getLangCode())) {
				return d;
			}
		}

		return null;
	}
}
