package no.hvl.dat152.test;

import static org.junit.Assert.*;

import org.junit.Test;

import no.hvl.dat152.utils.StringUtil;

public class Tests {
	@Test
	public void testRomanNumerals() {
		assertEquals("IX", StringUtil.romanNumeral(9));
		assertEquals("X", StringUtil.romanNumeral(10));
		assertEquals("MMXVII", StringUtil.romanNumeral(2017));
	}
	
	@Test
	public void testShortText() {
		assertEquals("asdasdasda ...", StringUtil.shortText("asdasdasdasdasd", 10));
	}
	
	@Test
	public void testCopyright() {
		assertEquals("© MMX-MMXVII HVL", StringUtil.copyright("HVL", 2010));
	}
	
	@Test
	public void testCapitalize() {
		assertEquals("Foo", StringUtil.capitalize("foo"));
	}
}
